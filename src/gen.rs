use rand::rngs::OsRng;
use regex::Regex;
use sha3::{Digest, Sha3_256};
use std::fs::{create_dir_all, File};
use std::io::{Result, Write};
use std::path::Path;
use std::sync::atomic::{AtomicU16, Ordering};
use std::sync::Arc;

const ALPHABET: &[u8] = b"abcdefghijklmnopqrstuvwxyz234567";
const SECRET_KEY_PREFIX: &[u8] = b"== ed25519v1-secret: type0 ==\0\0\0";
const PUBLIC_KEY_PREFIX: &[u8] = b"== ed25519v1-public: type0 ==\0\0\0";
const CHECKSUM_PREFIX: &[u8] = b".onion checksum";
const VERSION_SUFFIX: [u8; 1] = [3];

pub fn start(prefix: &Regex, output_dir: &Path, atomic: Arc<AtomicU16>) -> Result<()> {
    loop {
        let (secret, public, address) = generate_address();
        let matches = prefix.is_match(&address);
        if matches {
            save_key(output_dir, secret, public, &address)?;
        }
        atomic.fetch_add(1, Ordering::Relaxed);
    }
}

fn generate_address() -> ([u8; 32], [u8; 32], String) {
    let mut rng = OsRng;
    let keypair = ed25519_dalek::SigningKey::generate(&mut rng);
    let public = keypair.verifying_key().to_bytes();
    let secret = keypair.to_bytes();
    let mut hasher = Sha3_256::new();
    hasher.update(&CHECKSUM_PREFIX);
    hasher.update(&public);
    hasher.update(&VERSION_SUFFIX);
    let checksum = hasher.finalize();
    let address = base32encode(&[&public[..], &checksum[..2], &[3]].concat());
    (secret, public, address)
}

fn save_key(output_dir: &Path, secret: [u8; 32], public: [u8; 32], address: &str) -> Result<()> {
    println!("address found: {:}.onion", address);

    let mut secret_path = output_dir.to_path_buf();
    secret_path.push(&address);
    create_dir_all(&secret_path)?;

    let mut hostname_path = secret_path.clone();
    hostname_path.push("hostname");
    let mut hostname = File::create(&hostname_path)?;
    let name = format!("{:}.onion", address);
    write!(hostname, "{}", name).unwrap();

    let mut public_path = secret_path.clone();
    public_path.push("hs_ed25519_public_key");
    let mut public_file = File::create(&public_path)?;
    public_file.write_all(PUBLIC_KEY_PREFIX)?;
    public_file.write_all(&public)?;
    secret_path.push("hs_ed25519_secret_key");
    let mut file = File::create(&secret_path)?;
    file.write_all(SECRET_KEY_PREFIX)?;
    file.write_all(&secret)?;
    Ok(())
}
fn base32encode(data: &[u8]) -> String {
    let mut res = Vec::with_capacity((data.len() + 3) / 4 * 5);
    for chunk in data.chunks(5) {
        let buf = {
            let mut buf = [0u8; 5];
            for (i, &b) in chunk.iter().enumerate() {
                buf[i] = b;
            }
            buf
        };
        res.push(ALPHABET[((buf[0] & 0xF8) >> 3) as usize]);
        res.push(ALPHABET[(((buf[0] & 0x07) << 2) | ((buf[1] & 0xC0) >> 6)) as usize]);
        res.push(ALPHABET[((buf[1] & 0x3E) >> 1) as usize]);
        res.push(ALPHABET[(((buf[1] & 0x01) << 4) | ((buf[2] & 0xF0) >> 4)) as usize]);
        res.push(ALPHABET[(((buf[2] & 0x0F) << 1) | (buf[3] >> 7)) as usize]);
        res.push(ALPHABET[((buf[3] & 0x7C) >> 2) as usize]);
        res.push(ALPHABET[(((buf[3] & 0x03) << 3) | ((buf[4] & 0xE0) >> 5)) as usize]);
        res.push(ALPHABET[(buf[4] & 0x1F) as usize]);
    }

    if data.len() % 5 != 0 {
        let len = res.len();
        let num_extra = 8 - (data.len() % 5 * 8 + 4) / 5;
        res.truncate(len - num_extra);
    }
    String::from_utf8(res).unwrap()
}
