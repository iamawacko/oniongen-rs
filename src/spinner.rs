use std::borrow::Cow;
use std::io::{stdout, Write};
use std::sync::{atomic::AtomicBool, Arc};
use std::thread::{self, JoinHandle};

use once_cell::sync::Lazy;
use paste::paste;

pub struct Spinner {
    thread_handle: Option<JoinHandle<()>>,
    still_spinning: Arc<AtomicBool>,
    spinner_frames: SpinnerFrames,
    #[allow(dead_code)]
    msg: Cow<'static, str>,
    stream: Streams,
}

impl Spinner {
    fn stop_spinner_thread(&mut self) {
        self.still_spinning
            .store(false, std::sync::atomic::Ordering::Relaxed);

        self.thread_handle
            .take()
            .expect("Stopping the spinner thread should only happen once")
            .join()
            .expect("Failed to join threads");
    }

    pub fn new<S, T>(spinner_type: S, msg: T) -> Self
    where
        S: Into<SpinnerFrames>,
        T: Into<Cow<'static, str>>,
    {
        Self::new_with_stream(spinner_type, msg, Streams::default())
    }

    pub fn new_with_stream<S, T>(spinner_type: S, msg: T, stream: Streams) -> Self
    where
        S: Into<SpinnerFrames>,
        T: Into<Cow<'static, str>>,
    {
        let still_spinning = Arc::new(AtomicBool::new(true));
        let spinner_frames = spinner_type.into();
        let msg = msg.into();

        let handle = thread::spawn({
            let still_spinning = Arc::clone(&still_spinning);
            let spinner_frames = spinner_frames.clone();
            let msg = msg.clone();
            move || {
                let frames = spinner_frames
                    .frames
                    .iter()
                    .cycle()
                    .take_while(|_| still_spinning.load(std::sync::atomic::Ordering::Relaxed));
                let mut last_length = 0;
                for frame in frames {
                    let frame_str = format!("{} {}", frame, msg);
                    delete_last_line(last_length, stream);
                    last_length = frame_str.bytes().len();
                    write!(stream, "{frame_str}");
                    stream.get_stream().flush().expect("failed to flush stream");

                    thread::sleep(std::time::Duration::from_millis(u64::from(
                        spinner_frames.interval,
                    )));
                }
                delete_last_line(last_length, stream);
            }
        });

        Self {
            thread_handle: Some(handle),
            still_spinning,
            spinner_frames,
            msg,
            stream,
        }
    }

    pub fn update_text<T>(&mut self, msg: T)
    where
        T: Into<Cow<'static, str>>,
    {
        self.stop_spinner_thread();
        let _replaced = std::mem::replace(
            self,
            Self::new_with_stream(self.spinner_frames.clone(), msg, self.stream),
        );
    }
}

#[derive(Default, Copy, Clone, Debug)]
pub enum Streams {
    #[default]
    Stdout,
}

impl Streams {
    pub fn get_stream(self) -> Box<dyn Write + Send + Sync> {
        match self {
            Streams::Stdout => Box::new(stdout()),
        }
    }

    pub fn write_fmt<T>(self, fmt: T)
    where
        T: std::fmt::Display,
    {
        write!(self.get_stream(), "{fmt}").expect("failed to write stream");
    }
}

#[derive(Debug, Clone)]
pub struct SpinnerFrames {
    pub frames: Vec<&'static str>,
    pub interval: u16,
}

macro_rules! spinner_frames {
  ( $name:expr, [ $( $frame:expr ),* ], $interval:expr ) => {
      paste! {
          pub struct [< $name:camel >];

          impl From<[< $name:camel >]> for SpinnerFrames {
              fn from(_: [< $name:camel >]) -> SpinnerFrames {
                  [< $name:upper >].clone()
              }
          }

          static [< $name:upper >]: Lazy<SpinnerFrames>
              = Lazy::new(|| SpinnerFrames {
                  interval: $interval,
                  frames: vec![$($frame),*]
          });
      }
  };
}

spinner_frames!("line", ["-", "\\", "|", "/"], 100);

pub fn delete_last_line(clear_length: usize, stream: Streams) {
    write!(stream, "\r");
    for _ in 0..clear_length {
        write!(stream, " ");
    }
    write!(stream, "\r");
}
