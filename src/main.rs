#![forbid(unsafe_code)]

mod spinner;

use clap::{arg, command, Arg};
use regex::Regex;
use std::env::current_dir;
use std::fs::canonicalize;
use std::path::PathBuf;
use std::sync::atomic::{AtomicU16, Ordering};
use std::sync::Arc;
use std::time::Duration;

mod gen;

const BANNER: &str = r#"
   ____        _                                         
  / __ \      (_)                                        
 | |  | |_ __  _  ___  _ __   __ _  ___ _ __    _ __ ___ 
 | |  | | '_ \| |/ _ \| '_ \ / _` |/ _ \ '_ \  | '__/ __|
 | |__| | | | | | (_) | | | | (_| |  __/ | | | | |  \__ \
  \____/|_| |_|_|\___/|_| |_|\__, |\___|_| |_| |_|  |___/
                              __/ |                      
                             |___/                       
"#;

fn main() {
    let mut cmd = command!()
        .arg(arg!([regex] "Sets the regex").required(false))
        .arg(
            Arg::new("output")
                .short('o')
                .long("output")
                .help("Directory to output to"),
        )
        .arg(
            Arg::new("thread")
                .short('t')
                .long("thread")
                .help("Number of worker threads. Default is CPU core count."),
        );
    let args = cmd.clone().get_matches();
    let regex = match args.get_one::<String>("regex") {
        Some(regex) => regex,
        None => {
            println!("{}", cmd.render_usage());
            return ();
        }
    };
    let output_dir = match args.get_one::<String>("output") {
        Some(val) => canonicalize(&PathBuf::from(val)).unwrap(),
        None => current_dir().unwrap(),
    };

    let number = match args.get_one::<String>("thread") {
        Some(val) => val.parse().unwrap_or(1),
        None => std::thread::available_parallelism()
            .expect("Failed to get default parallelism")
            .get(),
    };

    println!("{}", BANNER);
    println!("Threads: {}", number);
    println!("Output Directory: {:?}", output_dir);
    println!("Regex: {}", regex);

    let prefix = Regex::new(regex).expect("Regex Error");

    let mut handle_list = vec![];

    let mut atomics = vec![];

    for _ in 0..number {
        let prefix = prefix.clone();
        let output_dir = output_dir.clone();
        let atomic = Arc::new(AtomicU16::new(0));
        atomics.push(atomic.clone());
        let handle = std::thread::spawn(move || gen::start(&prefix, &output_dir, atomic).unwrap());
        handle_list.push(handle);
    }

    let mut counter: u64;
    let mut spinning = spinner::Spinner::new(spinner::Line, "Starting...");
    loop {
        counter = 0;

        for atomic in &atomics {
            counter += atomic.swap(0, Ordering::Relaxed) as u64;
        }

        spinning.update_text(format!("{counter} combinations / second"));
        std::thread::sleep(Duration::from_secs(1));
    }
}
