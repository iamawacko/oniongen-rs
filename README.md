# Oniongen-rs
[![dependency status](https://deps.rs/crate/oniongen-rs/0.6.5/status.svg)](https://deps.rs/crate/oniongen-rs/0.6.5)

A v3 .onion address vanity URL generator written in Rust.

This implementation generates random ed25519 keys on all CPU cores. If the address generated matches your prefix, the hostname, public key, and private key are written to a new directory.

## Installation

You can grab the latest executables from the latest [release](https://gitlab.com/iamawacko/oniongen-rs/-/releases).

You could also compile it yourself with:

```
$ cargo install oniongen-rs
```
or for bleeding edge changes:

```bash
$ git clone https://gitlab.com/iamawacko/oniongen-rs
$ cd oniongen-rs
$ cargo build --release
$ cp target/release/oniongen-rs ~/.local/bin # assuming ~/.local/bin is in your path
```

## Usage

```
$ oniongen-rs -t [threads] -o [output] [regex]
```
Only the regex is required. The thread flag is the number of worker threads that will. The default is the number of CPU cores.. The output flag is where to output the directories. The default is the current directory.

## Example

```
$ oniongen-rs -t 10 -o data/ ^rust
```

## MSRV
1.64.0

## Todo

- [x] Address Generation
- [x] Multi-threading
- [x] Banner
- [x] Regex Support
- [ ] Package for Other Systems
- [ ] Make a Docker Image
